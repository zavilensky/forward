<?php

namespace football;

class Forward
{
    public function __construct()
    {
    }

    public function  hit()
    {
        $resultHit = random_int(1, 3);

        switch ($resultHit) {
            case 1:
                return $this->hitHead();
            case 2:
                return $this->hitFoot();
            case 3:
                return $this->hitHand();
        }
    }

    private function hitHead()
    {
        $resultHit = random_int(1, 5);

        if (1 === $resultHit) {
            return "goal";
        } else {
            return "slip";
        }
    }

    private function hitFoot()
    {
        $resultHit = random_int(1, 2);

        if (1 === $resultHit) {
            return "goal";
        } else {
            return "slip";
        }
    }

    private function hitHand()
    {
        return "slip";
    }
}